# ExpoNotif

Juste un petit utilitaire qu'on peut mettre dans programme serveur
codé en Python, pour envoyer de facon automatique, des notifications
au utilisateur utilisant une application android

## Installation

Dans un environement virtuel, installez les dépendances contenu
dans le fichier requirement.

```
pip install -r requirement
```

## Utilisation

```python
# Python 3.8.10

# on importe
import exponotif as expn

# le token de Expo généré depuis l'application de l'utilisateur
token = "ExponentPushToken[-HcG2aEOizPLf65DrlM0t7]";

# le message à envoyer ne notification
message = "Cc guys !";

print('Envoie de notification ...');
# on tante d'envoyer la notification
sending_ok = expn.send_push_message(token, message);

if sending_ok:
    print('Notification envoyée !');

else:
    print("Echec de l'envoie !");

```

# Plus d'information

Pour plus d'information, voici quelque lien :
- [Documentation de l'API de expo](https://docs.expo.io/versions/latest/guides/push-notifications#http2-api)
- [Code source](https://github.com/expo-community/expo-server-sdk-python)
