# Python 3.8.10

# on importe
import exponotif as expn

# le token de Expo généré depuis l'application de l'utilisateur
token = "ExponentPushToken[-HcG2aEOizPLf65DrlM0t7]";

# le message à envoyer ne notification
message = "Cc guys !";

print('Envoie de notification ...');
# on tante d'envoyer la notification
sending_ok = expn.send_push_message(token, message);

if sending_ok:
    print('Notification envoyée !');

else:
    print("Echec de l'envoie !");