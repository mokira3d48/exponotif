from exponent_server_sdk import DeviceNotRegisteredError
from exponent_server_sdk import PushClient
from exponent_server_sdk import PushMessage
# from exponent_server_sdk import PushResponseError
from exponent_server_sdk import PushServerError
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError


# Arguments de base. Vous devez étendre cette fonction avec les fonctionnalités
# push que vous souhaitez utiliser, ou simplement passer un objet `PushMessage`.
def send_push_message(token, message, extra=None):
    try:
        push_message = PushMessage(to=token, body=message, data=extra);
        response     = PushClient().publish(push_message);

        return True;

    except PushServerError as exc:
        print("PUSH SERVER ERROR !");

        # A rencontré une erreur probable de formatage/validation. 
        rollbar.report_exc_info(
            extra_data={
                'token': token,
                'message': message,
                'extra': extra,
                'errors': exc.errors,
                'response_data': exc.response_data,
            });
        raise

    except (ConnectionError, HTTPError) as exc:
        print("CONNECTION ERROR !");

        # A rencontré une erreur de connexion ou HTTP - réessayez plusieurs fois
        # au cas où cela serait transitoire.
        rollbar.report_exc_info(extra_data={
            'token': token, 
            'message': message, 
            'extra': extra
        });
        raise self.retry(exc=exc);

    try:
        # Nous avons reçu une réponse, mais nous ne savons pas encore s'il s'agit 
        # d'une erreur. Cet appel génère des erreurs afin que nous puissions 
        # les gérer avec des flux d'exception normaux .
        response.validate_response();

    except DeviceNotRegisteredError:
        print("DEVICE NOT REGISTERED ERROR !");

        # Marque le jeton push comme inactif
        from notifications.models import PushToken
        PushToken.objects.filter(token=token).update(active=False);

    # except PushResponseError as exc:
    #     # Rencontré une autre erreur par notification. 
    #     rollbar.report_exc_info(
    #         extra_data={
    #             'token': token,
    #             'message': message,
    #             'extra': extra,
    #             'push_response': exc.push_response._asdict(),
    #         })
    #     raise self.retry(exc=exc)

    # on renvoie faux
    return False;

